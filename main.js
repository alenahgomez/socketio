// Load requirements
var http = require('http'), //servidor web
io = require('socket.io'); //socket
var count = 0; //contador de cantidad de conexiones
var mysql = require('mysql'); //conector a la db
var conexionesActuales = {}; //creo un diccionario donde almaceno las conexiones actuales


// Crear conexion a la db
var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "test"
});

//establezco conexion a la db
con.connect(function(err) {
    if (err) console.log(err);
    console.log("Conectado a la db!");
});

//Creo un servidor http
var server = http.createServer(function(req, res)
{
    // Send HTML headers and message
    res.writeHead(404, {'Content-Type': 'text/html'});
    res.end('<h1>Aw, snap! 404</h1>');
});
//lo pongo a escuchar en el puerto 8080
server.listen(8080);
//instancio un socket escuchando en ese server
io = io.listen(server);

//Cuando el socket recibe una conexion
io.sockets.on('connection', function(socket)
{
    console.log('Cliente conectado con ID: ' + socket.id); //en el server muestro el ID del cliente
    nro_expediente = socket.handshake.query.nro_expediente; //recibo un parametro por query llamado nro_expediente
    if (nro_expediente !== undefined) { //si este parametro existe entonces
        conexionesActuales[socket.id] = {nro_expediente : nro_expediente}; //lo guardo en diccionario de conexiones actuales
        console.log("Expediente en uso: " + conexionesActuales[socket.id].nro_expediente); //muestro de onda en el server
        con.query("UPDATE expedientes SET Estado = 1 WHERE nro_expediente = ?", conexionesActuales[socket.id].nro_expediente, (err, res) => {
            if (err) console.log(err);
            console.log("Bloqueado: " + conexionesActuales[socket.id].nro_expediente);
        }); //seteo el expediente actual en la db con estado bloqueado
        io.emit('bloqueado', conexionesActuales[socket.id].nro_expediente); //emito para todas las conexiones activas un llamado a la funcion de bloqueo con el expediente actual
    }
    count = count + 1; //sumo en el contador una conexion activa
    io.emit('cantidad', count) //emito la cantidad de conexiones, no necesario, solo para propositos de prueba

    socket.on('disconnect', function() { //si detecto el evento desconexion para algun socket
        console.log('Cliente desconectado con ID: ' + socket.id); //muestro en consola del server
        if (conexionesActuales[socket.id] !== undefined) { //si estaba almacenado en las conexiones actuales
            console.log("Expediente liberado: " + conexionesActuales[socket.id].nro_expediente); //libero el expediente
            con.query("UPDATE expedientes SET Estado = 0 WHERE nro_expediente = ?", conexionesActuales[socket.id].nro_expediente, (err, res) => {
                if (err) console.log(err);
                console.log("Desbloqueado: " + conexionesActuales[socket.id].nro_expediente);
            }); //con esta query libero el expediente
            io.emit('desbloqueado', conexionesActuales[socket.id].nro_expediente); //emito un llamado a la funcion de desbloqueo para ese expediente
        }
        count = count - 1; //
        io.emit('cantidad', count)
    });
});
