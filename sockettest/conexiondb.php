<?php 
  $conn = new mysqli("localhost", "root", "", "test");
  if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
  }
  echo "Connected successfully";
  $sql = "SELECT * FROM expedientes";
  $result = $conn->query($sql);
  if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        echo "<tr><td>".$row["nro_expediente"]."</td><td>".$row["descripcion"]."</td><td>".$row["fechaAlta"]."</td>";
        echo "<td class=''>";
        if ($row["Estado"] == 0) {
            echo "<span class='alert alert-success' id=".$row["nro_expediente"].">LIBRE</span></td>";
        } else {
            echo "<span class='alert alert-danger' id=".$row["nro_expediente"].">BLOQUEADO</span></td>";
        }
        
        echo "<td><a class='alert alert-primary' href='expediente.php?nro_expediente=".$row["nro_expediente"]."'>---</a></td></tr>";
    }
    echo "</table>";
  } else {
      echo "0 results";
  }
  $conn->close();
?>