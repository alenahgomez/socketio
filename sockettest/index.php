<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Test</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="js/socket.io.js"></script>
</head>
<body>
    <h1>Test socket</h1>
    <table class="table">
        <thead>
          <tr>
            <th>Número</th>
            <th>Descripción</th>
            <th>Fecha</th>
            <th>Estado</th>
            <th>Ingresar</th>
          </tr>
        </thead>
        <tbody>
        <?php
          include 'conexiondb.php';
        ?>
        </tbody>
    </table>

    <script>
      var socket = io.connect('http://localhost:8080/', {'force new connection': true});

      socket.on('cantidad', function (data) {
        console.log("Cantidad de conectados: " + data);
      });

      socket.on('bloqueado', function (data) {
        console.log("Recibido Bloqueado: " + data);
        console.log(document.getElementById(data));
        //document.getElementById(data).classList.remove = "alert-success";
        removeClass(data, "alert-success");
        //document.getElementById(data).classList.add = "alert-danger";
        addClass(data, "alert-danger");
        document.getElementById(data).innerHTML = "BLOQUEADO";
      });

      socket.on('desbloqueado', function (data) {
        console.log("Recibido Desbloqueado: " + data);
        console.log(document.getElementById(data));
        //document.getElementById(data).classList.remove = "alert-danger";
        removeClass(data, "alert-danger");
        //document.getElementById(data).classList.add = "alert-success";
        addClass(data, "alert-success");
        document.getElementById(data).innerHTML = "LIBRE";
      });

      function addClass(id, name) {
        var element, arr;
        element = document.getElementById(id);
        arr = element.className.split(" ");
        if (arr.indexOf(name) == -1) {
          element.className += " " + name;
        }
      }
      function removeClass(id, name) {
        var element = document.getElementById(id);
        element.className = element.className.replace(name, "");
      } 
    </script>

</body>
</html>